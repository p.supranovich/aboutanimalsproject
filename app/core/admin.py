from django.contrib import admin
from .models import Animal,Species,Sex,Routine,Parents

admin.site.register(Animal)
admin.site.register(Species)
admin.site.register(Sex)
admin.site.register(Routine)
admin.site.register(Parents)

