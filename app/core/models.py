from django.db import models
from django.utils import timezone
import datetime

class Animal(models.Model):
    name=models.CharField(max_length=200)
    species = models.ForeignKey('Species', on_delete=models.CASCADE)
    dateOfBirth = models.DateField(default=timezone.now)
    sex = models.ForeignKey('Sex', on_delete=models.CASCADE)
    parents = models.ForeignKey('Parents', on_delete=models.SET_NULL, blank=True, null=True)
    def __str__(self):
        return "{0} , {1}".format(self.name,self.species)

class Species(models.Model):
    name = models.CharField(max_length=200)
    nameLatin = models.CharField(max_length=200)

    def __str__(self):
        return "{0}".format(self.name)

class Sex(models.Model):
    name=name = models.CharField(max_length=20)

    def __str__(self):
        return "{0}".format(self.name)

class Routine(models.Model):
    name = models.CharField(max_length=200)
    routineType = models.CharField(max_length=200)
    startsAt = models.DateTimeField(default=datetime.datetime.now())
    animal = models.ForeignKey('Animal', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "{0} | {1}".format(self.name, self.animal)

class Parents(models.Model):
    mother=models.ForeignKey('Animal',on_delete=models.CASCADE, null=True,blank=True, related_name='mother')
    father = models.ForeignKey('Animal',on_delete=models.CASCADE,null=True, blank=True, related_name='father')

    def __str__(self):
        return "{0} & {1}".format(self.mother.name, self.father.name)