from django.urls import path
from . import views

app_name='animals'
urlpatterns = [

    path('', views.Main.as_view(), name="main"),

    path('animal/', views.MainAnimal.as_view(), name="main_animal"),
    path('animal/<int:pk>', views.DetailAnimal.as_view(), name="detail_animal" ),
    path('animal/create/', views.AnimalCreate.as_view(), name='create_animal'),
    path('animal/<int:pk>/update/', views.AnimalUpdate.as_view(), name='update_animal'),
    path('animal/<int:pk>/delete/', views.AnimalDelete.as_view(), name='delete_animal'),

    path('routine/', views.MainRoutine.as_view(), name="main_routine"),
    path('routine/<int:pk>', views.DetailRoutine.as_view(), name="detail_routine"),
    path('routine/create/', views.RoutineCreate.as_view(), name='create_routine'),
    path('routine/<int:pk>/update/', views.RoutineUpdate.as_view(), name='update_routine'),
    path('routine/<int:pk>/delete/', views.RoutineDelete.as_view(), name='delete_routine'),


]