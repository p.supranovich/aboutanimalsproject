from django.template import loader
from django.contrib import messages
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import CreateView, UpdateView, DeleteView, ListView, DetailView,View
from .models import Animal,Routine
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
# example
#def index(request):
    #template = loader.get_template('core/animal_main.html')
    #return HttpResponse(template.render({}, request))


#############################  MAIN  #########################################

class Main(View):
    template = 'core/main.html'
    success_url = reverse_lazy('animal:main')

    def get(self, request):
        ctx = {'countAnimals': Animal.objects.count(),'countRoutines': Routine.objects.count()}
        return render(request, self.template, ctx)

#############################  ANIMALS  #########################################
class MainAnimal(ListView):
    model= Animal
    template_name = "core/animal_main.html"

class DetailAnimal(LoginRequiredMixin,DetailView):
    model = Animal
    template_name="core/animal_detail.html"

    def get_context_data(self, *args, **kwargs):
        ctx = super(DetailAnimal, self).get_context_data(*args, **kwargs)
        ctx['childs_list'] = Animal.objects.all()
        return ctx

class AnimalCreate(LoginRequiredMixin, CreateView):
    model = Animal
    fields = '__all__'
    success_url = reverse_lazy('animals:main_animal')

class AnimalUpdate(LoginRequiredMixin, UpdateView):
    model = Animal
    fields = '__all__'
    success_url = reverse_lazy('animals:main_animal')

class AnimalDelete(LoginRequiredMixin, DeleteView):
    model = Animal
    fields = '__all__'
    success_url = reverse_lazy('animals:main_animal')

#############################  ROUTINES  #########################################

class MainRoutine(ListView):
    model= Routine
    template_name = "core/routine_main.html"

class DetailRoutine(LoginRequiredMixin,DetailView):
    model = Routine
    template_name="core/routine_detail.html"

class RoutineCreate(LoginRequiredMixin, CreateView):
    model = Routine
    fields = '__all__'
    success_url = reverse_lazy('animals:main_routine')

class RoutineUpdate(LoginRequiredMixin, UpdateView):
    model = Routine
    fields = '__all__'
    success_url = reverse_lazy('animals:main_routine')

class RoutineDelete(LoginRequiredMixin, DeleteView):
    model = Routine
    fields = '__all__'
    success_url = reverse_lazy('animals:main_routine')